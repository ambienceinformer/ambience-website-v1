<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Dashboard</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="style.css">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>

		
		<div class="site-content">
			<div class="site-header">
				<div class="container">
					<a href="#" class="branding">
						<img src="images/logo.png" alt="" class="logo">
						<div class="logo-type">
							<h1 class="site-title">Ambience Informer</h1>
							<small class="site-description">Ambience on the go...!</small>
						</div>
					</a>

					<!-- Default snippet for navigation -->
					<div class="main-navigation">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item"><a href="index.html">Logout</a></li>
						</ul> <!-- .menu -->
					</div> <!-- .main-navigation -->

					<div class="mobile-navigation"></div>

				</div>
			</div> <!-- .site-header -->

					</div>
				</div>
			</div>
			
			<main class="main-content">
			<div class="container" >

				<form action="#" class="find-location2">
						<input class="grey" type="text" placeholder="Location / Device ID">
						<input type="submit" value="Search">
					</form>

				<div class="tbcss">
				<table class="tb">
					<tr>
						<th class="top-left">Device ID</th>
						<th>Location</th>
						<th>Temperature (<sup>o</sup>C)</th>
						<th>Humidity (%)</th>
						<th>Rain Status</th>
						<th>Air Pressure (mbar)</th>
						<th class="top-right">Battery (%)</th>
					</tr>

					<?php 
					$GLOBALS['msg']='';
					$connection = new mysqli ('fdb16.awardspace.net','2411407_birju','ambience1234','2411407_birju');
					if($connection -> connect_error) 
					{
						$GLOBALS['msg'] = 'Failed to connect,try again!';
					}
					else
					{
						$sql = "select * from weatherinfo";
						$rs = $connection -> query($sql);
						if($rs -> num_rows != 0 )
						{                       
							while($row = $rs->fetch_assoc()) 
							{
								echo "<tr>";
								echo "<td>".$row["deviceId"]."</td>";
								echo "<td>".$row["location"]."</td>";
								echo "<td>".$row["temperature"]."</td>";
								echo "<td>".$row["humidity"]."</td>";
								echo "<td>".$row["rainStatus"]."</td>";
								echo "<td>".$row["airPressure"]."</td>";
								echo "<td>".$row["battery"]."</td>";
								echo "</tr>";	
							}
						}
						else
						{
							$GLOBALS['msg'] = 'No data!';	
						}
					}

					?>
										
				</table>
				<p id="msg"><?php echo $msg ?></p>
				</div>
				</div>
			</main> <!-- .main-content -->

			<footer class="site-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							
						</div>
						<div class="col-md-3 col-md-offset-1">
							<div class="social-links">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-pinterest"></i></a>
							</div>
						</div>
					</div>

					<p class="colophon">Designed by Ambience Informer Inc.</p>
				</div>
			</footer> <!-- .site-footer -->
		</div>
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>

</html>