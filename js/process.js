function refresh() 
{
	var search=document.getElementById('device_id').value;
	if(search === '')
	{
		document.getElementById('error').innerHTML="Find a valid device first";
	}
	else
	{
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
    	   if (this.readyState == 4 && this.status == 200) 
            {
               	var values = this.responseText.split(",");
                document.getElementById('d_id').innerHTML=values[0];
                document.getElementById('d_loc').innerHTML=values[1];
                document.getElementById('d_temp').innerHTML=values[2]+"<sup>o</sup>C";
                document.getElementById('d_humidity').innerHTML=values[3]+"%";
                document.getElementById('d_rain').innerHTML=values[4];
                document.getElementById('d_air').innerHTML=values[5]+"<br>mbar";
                document.getElementById('d_battery').innerHTML=values[6]+"%";
                document.getElementById('error').innerHTML="";
            }
        };
        var url="php/refresh.php?q="+document.getElementById('device_id').value;
        xhttp.open("GET", url, true);
        xhttp.send(); 
    }
}

function find()
{
	var search=document.getElementById('find_id').value;
	if(search === '')
	{
		document.getElementById('error').innerHTML="Enter a device first";
	}
	else
	{
		var xhttp = new XMLHttpRequest();
   		xhttp.onreadystatechange = function() 
        {
        	if (this.readyState == 4 && this.status == 200) 
            {
                var values = this.responseText.split(",");
                if(values.length ==2 )
                {
			 	   document.getElementById('error').innerHTML=values[1];
                    document.getElementById('device_id').value="";
                    document.getElementById('d_id').innerHTML="N/A";
                    document.getElementById('d_loc').innerHTML="N/A";
                    document.getElementById('d_temp').innerHTML="N/A";
                    document.getElementById('d_humidity').innerHTML="N/A";
                    document.getElementById('d_rain').innerHTML="N/A";
                    document.getElementById('d_air').innerHTML="N/A";
                    document.getElementById('d_battery').innerHTML="N/A";
                }
                else
                {
            	   var values = this.responseText.split(",");
                    document.getElementById('d_id').innerHTML=values[0];
                    document.getElementById('d_loc').innerHTML=values[1];
                    document.getElementById('d_temp').innerHTML=values[2]+"<sup>o</sup>C";
                    document.getElementById('d_humidity').innerHTML=values[3]+"%";
                    document.getElementById('d_rain').innerHTML=values[4];
                    document.getElementById('d_air').innerHTML=values[5]+"<br>mbar";
                    document.getElementById('d_battery').innerHTML=values[6]+"%";
                    document.getElementById('error').innerHTML="";
                    document.getElementById('device_id').value=values[0];
                }
            }
        };
        var url="php/find.php?q="+document.getElementById('find_id').value;
        xhttp.open("GET", url, true);
        xhttp.send();
	}
}


function new_device()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {    
        if (this.readyState == 4 && this.status == 200) 
        {
            var values = this.responseText;
            document.getElementById('msg1').innerHTML=values;
            
        }
    };
    var url="php/new.php?id="+document.getElementById('n_id').value+"&loc="+document.getElementById('n_loc').value;
    xhttp.open("GET", url, true);
    xhttp.send();
}

function change()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {    
        if (this.readyState == 4 && this.status == 200) 
        {
            var values = this.responseText;
            document.getElementById('msg2').innerHTML=values;
        }    
    };
    var url="php/change.php?id="+document.getElementById('c_id').value+"&loc="+document.getElementById('c_loc').value;
    xhttp.open("GET", url, true);
    xhttp.send();
}

function login()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {    
        if (this.readyState == 4 && this.status == 200) 
        {
            var values = this.responseText;
            if(values=== 'done')
            {
                window.location = "dashboard.php";
            }
            else
            {
                document.getElementById('msg').innerHTML=values;
            }
        }    
    };
    var url="php/login.php?id="+document.getElementById('a_id').value+"&pass="+document.getElementById('a_pass').value;
    xhttp.open("GET", url, true);
    xhttp.send();
}